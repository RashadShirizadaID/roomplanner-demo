//
//  RoomObjectCell.swift
//  RoomPlanner
//
//  Created by Rashad Shirizada on 13.10.22.
//

import UIKit
import RoomPlan

class RoomObjectCell: UITableViewCell {
    @IBOutlet weak var categoryLbl: UILabel!
    @IBOutlet weak var identifierLbl: UILabel!
    @IBOutlet weak var confidenceLbl: UILabel!
    @IBOutlet weak var dimensionsLbl: UILabel!

    
    func updateUI(object: CapturedRoom.Object) {
        categoryLbl.text = "\(object.category)"
        identifierLbl.text = object.identifier.description
        confidenceLbl.text = "\(object.confidence.hashValue)"
        
        var dimension = String(format: "%@ = %f, %@ = %f, %@ = %f", "x",object.dimensions.x, "y", object.dimensions.y, "z", object.dimensions.z)
        dimensionsLbl.text = dimension
    }

}
