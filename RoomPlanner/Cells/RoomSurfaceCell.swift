//
//  RoomSurfaceCell.swift
//  RoomPlanner
//
//  Created by Rashad Shirizada on 13.10.22.
//

import RoomPlan
import UIKit

class RoomSurfaceCell: UITableViewCell {

    @IBOutlet weak var categoryLbl: UILabel!
    @IBOutlet weak var identifierLbl: UILabel!
    @IBOutlet weak var confidenceLbl: UILabel!
    @IBOutlet weak var dimensionsLbl: UILabel!
    
    func updateUI(surface: CapturedRoom.Surface) {
        categoryLbl.text = "\(surface.category)"
        identifierLbl.text = surface.identifier.description
        confidenceLbl.text = "\(surface.confidence.hashValue)"
        
        var dimension = String(format: "%@ = %f, %@ = %f, %@ = %f", "x",surface.dimensions.x, "y", surface.dimensions.y, "z", surface.dimensions.z)
        dimensionsLbl.text = dimension
        
//        surface.dimensions.
    }

}
