//
//  OnboardingViewController.swift
//  RoomPlanner
//
//  Created by Rashad Shirizada on 13.10.22.
//

import UIKit

class OnboardingViewController: UIViewController {
    var calculator = RoomFloorCalculator()
    override func viewDidLoad() {
        super.viewDidLoad()
        
//        openMockRoomData()
        
    }
    
    private func openMockRoomData() {
        if let bundlePath = Bundle(for: OnboardingViewController.self).path(forResource: "room",
                                                                            ofType: "json"),
           let jsonData = try? String(contentsOfFile: bundlePath).data(using: .utf8),
           let roomData = try? JSONDecoder().decode(RoomData.self, from: jsonData) {

            let roomModelController = RoomModelController(roomData: roomData)
            self.navigationController?.pushViewController(roomModelController, animated: true)
            
//            var results = calculator.calculate(data: roomData)
            
        }
    }
    
    
    @IBAction func startScan(_ sender: UIButton) {
        if let viewController = self.storyboard?.instantiateViewController(
            withIdentifier: "RoomCaptureController") {
            
            viewController.modalPresentationStyle = .fullScreen
            present(viewController, animated: true)
        }
    }
    
}
