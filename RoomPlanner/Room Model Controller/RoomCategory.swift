//
//  RoomCategory.swift
//  RoomPlanner
//
//  Created by Rashad Shirizada on 22.11.22.
//

import Foundation
import Foundation
import SceneKit

private class Dummy {

}
enum RoomCategory: Decodable {
    case door
    case window
    case wall
    case opening
    case objects
    case storage
    case refrigerator
    case stove
    case bed
    case sink
    case washerDryer
    case toilet
    case bathtub
    case oven
    case dishwasher
    case table
    case sofa
    case chair
    case fireplace
    case television
    case stairs

    var desc: String {
        switch self {
        case .door:
            return "Door"
        case .window:
            return "Window"
        case .wall:
            return "Wall"
        case .opening:
            return "Opening"
        case .objects:
            return "Object"
        default:
            return "\(self)"
        }
    }

    var color: UIColor? {
        switch self {
//        case .door:
//            return .black
//        case .window:
//            return .blue
//        case .wall:
//            return .red
//        case .opening:
//            return .gray
        default:
            return .red
        }
    }

    var node: SCNNode? {

        switch self {
//        case .chair:
//            var bundle = Bundle(for: Dummy.self)
//        var furnitureName = "Chair"
//            if let url = bundle.url(forResource: "Chair", withExtension: "scn"), let scene = try? SCNScene(url: url) {
//                let node = (scene.rootNode.childNode(withName: furnitureName, recursively: false))!
//
//                node.geometry?.firstMaterial?.diffuse.contents = UIColor.green
//                return node
//            }
//            return nil
        default:
            return nil
        }
    }
    var length: Float {
        switch self {
        case .door:
            return  0.5
        case .window:
            return 0.4
        case .wall:
            return 0.2
        case .opening:
            return 0.4
        default:
            return 0.2
        }
    }
}

enum RoomConfidence: Decodable {
    case high
    case medium
    case low

    var desc: String {
        switch self {
        case .high:
            return "High"
        case .medium:
            return "Medium"
        case .low:
            return "Low"
        }
    }
}
