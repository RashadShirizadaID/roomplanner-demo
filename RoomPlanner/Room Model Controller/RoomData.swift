//
//  RoomData.swift
//  RoomPlanner
//
//  Created by Rashad Shirizada on 22.11.22.
//


import Foundation
import SceneKit
struct RoomData: Decodable {
    var doors: [RoomItem]?
    var walls: [RoomItem]?
    var openings: [RoomItem]?
    var windows: [RoomItem]?
    var objects: [RoomItem]?

    var allItems: [RoomItem] {
        var items: [RoomItem] = []

        items.append(contentsOf: doors ?? [])
        items.append(contentsOf: walls ?? [])
        items.append(contentsOf: openings ?? [])
        items.append(contentsOf: windows ?? [])
        items.append(contentsOf: objects ?? [])

        return items
    }

    var sectionedData: [String: [RoomItem]] {
        var totalItems: [String: [RoomItem]] = [:]

        if let doors = doors, !doors.isEmpty {
            totalItems.updateValue(doors, forKey: "Doors")
        }

        if let windows = windows, !windows.isEmpty {
            totalItems.updateValue(windows, forKey: "Windows")
        }

        if let walls = walls, !walls.isEmpty {
            totalItems.updateValue(walls, forKey: "Walls")
        }

        if let openings = openings, !openings.isEmpty {
            totalItems.updateValue(openings, forKey: "Openings")
        }

        if let objects = objects, !objects.isEmpty {
            totalItems.updateValue(objects, forKey: "Objects")
        }

        return totalItems
    }
}

struct RoomItem: Decodable {
    var category: RoomCategory?
    var confidence: RoomConfidence?
    var dimensions: [Float]?
    var identifier: String?
    var parentIdentifier: String?
    var transform: [Float]?

    var dimensionVector: SCNVector3? {
        if let vector = dimensions {
            return SCNVector3Make(vector[0], vector[1], vector[2])
        }
        return nil
    }

    var scnTransform: SCNMatrix4? {
        guard let transforms = transform else { return nil }
        return SCNMatrix4(m11: transforms[0], m12: transforms[1], m13: transforms[2], m14: transforms[3],
                          m21: transforms[4], m22: transforms[5], m23: transforms[6], m24: transforms[7],
                          m31: transforms[8], m32: transforms[9], m33: transforms[10], m34: transforms[11],
                          m41: transforms[12], m42: transforms[13], m43: transforms[14], m44: transforms[15])
    }

    var node: SCNNode {
        let width = dimensions?[0] ?? 0.0
        let height = dimensions?[1] ?? 0.0
        let length = dimensions?[2] ?? 0.0

        let boxShape = SCNBox(
            width: CGFloat(width),
            height: CGFloat(height),
            length: CGFloat(length), // CGFloat(category?.length ?? 0.0),
            chamferRadius: 0
        )
        boxShape.firstMaterial?.diffuse.contents = category?.color
        boxShape.firstMaterial?.locksAmbientWithDiffuse = false

        // Generate new SCNNode
        let newNode = SCNNode(geometry: boxShape)
        if let transform = scnTransform {
            newNode.transform = transform
        }

        newNode.name = identifier

        return newNode
    }
    //    }

    var seachKey: String {
        switch self.category {
        case .wall:
            return "\(category?.desc ?? "") decor"
        default:
            return category?.desc ?? ""
        }
    }
}

extension RoomItem: RoomItemProtocol {
    var roomCategory: RoomCategory? {
        return category
    }

    var roomIdentifier: String? {
        return identifier
    }

    var roomConfidence: RoomConfidence? {
        return confidence
    }

    var roomDimensions: [Float]? {
        return dimensions
    }

}

protocol RoomItemProtocol {
    var roomCategory: RoomCategory? { get }
    var roomIdentifier: String? { get }
    var roomConfidence: RoomConfidence? { get }
    var roomDimensions: [Float]? { get }
}
extension RoomItemProtocol {
    var dimensionDesc: String? {
        let dimension = String(format: "%@ = %f, %@ = %f, %@ = %f", "x", roomDimensions?[0] ?? "", "y", roomDimensions?[1] ?? "", "z", roomDimensions?[2] ?? "")
        return dimension
    }
}


