//
//  RoomFloorCalculator.swift
//  RoomPlanner
//
//  Created by Rashad Shirizada on 22.11.22.
//

import Foundation
import RoomPlan
import SceneKit

struct RoomFloorCalculator {
    
    
    func calculate(data: RoomData) {
        data.walls?.forEach({ w in
            //            print("WALL::")
            //            print("transform", w.transform)
            //            print("dimension",w.dimensions)
            //            print("\n")
        })
    }
}

extension SCNGeometry {
    
    class func triangleFrom(vector1: SCNVector3, vector2: SCNVector3, vector3: SCNVector3) -> SCNGeometry {
        
        let indices: [Int32] = [0, 1, 2]
        
        let source = SCNGeometrySource(vertices: [vector1, vector2, vector3])
        
        let element = SCNGeometryElement(indices: indices, primitiveType: .triangles)
        
        return SCNGeometry(sources: [source], elements: [element])
    }
    
    class func form(vectors: [SCNVector3]) -> SCNGeometry {
        
        var indices: [Int32] = []
        for i in 0..<vectors.count {
            indices.append(Int32(i))
        }
        
        let source = SCNGeometrySource(vertices: vectors)
        
        let element = SCNGeometryElement(indices: indices, primitiveType: .polygon)
        
        return SCNGeometry(sources: [source], elements: [element])
    }
    
    
    class func lineFrom(vector vector1: SCNVector3, toVector vector2: SCNVector3) -> SCNGeometry {
        let indices: [Int32] = [0, 1]
        
        let source = SCNGeometrySource(vertices: [vector1, vector2])
        let element = SCNGeometryElement(indices: indices, primitiveType: .line)
        
        return SCNGeometry(sources: [source], elements: [element])
        
    }
    
    
    static func polygonPlane(vertices: [SCNVector3]) -> SCNGeometry {
        var indices: [Int32] = [Int32(vertices.count)]
        indices.append(contentsOf: generateIndices(max: vertices.count))
        let vertexSource = SCNGeometrySource(vertices: vertices )
        let indexData = Data(bytes: indices,
                             count: indices.count * MemoryLayout<Int32>.size)
        let element = SCNGeometryElement(data: indexData,
                                         primitiveType: .polygon,
                                         primitiveCount: 1,
                                         bytesPerIndex: MemoryLayout<Int32>.size)
        return SCNGeometry(sources: [vertexSource], elements: [element])
    }
    
    private static func generateIndices(max maxIndexValue: Int) -> [Int32]{
            var counter: Int = 0
            var output: [Int32] = []
            while counter < maxIndexValue {
                output.append(Int32(counter))
                counter += 1
            }
            return output
        }
}
