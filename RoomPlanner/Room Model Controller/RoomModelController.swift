//
//  RoomModelController.swift
//  RoomPlanner
//
//  Created by Rashad Shirizada on 25.10.22.
//

import UIKit
import SceneKit
import RoomPlan
import SwiftUI

class RoomModelController: UIViewController {
    
    //    var domainModel: RoomScannerDomainModel
    @IBOutlet weak var sceneView: SCNView!
    var roomData: RoomData
    
    init(roomData: RoomData) {
        self.roomData = roomData
        //        self.domainModel = domainModel
        super.init(nibName: "RoomModelController", bundle: Bundle(for: RoomModelController.self))
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.sceneView.delegate = self
        
//        addObjects()
        addObjects2()
        
    }
    
    private func addObjects() {
        let bundle = Bundle(for: RoomModelController.self)
        guard let url = bundle.url(forResource: "Floor", withExtension: "scn"),
              let scene = try? SCNScene(url: url)
        else { return }
        
        
        let rootNode = scene.rootNode
        for i in 0...(roomData.allItems.endIndex - 1) {
            
            let node = roomData.allItems[i].node
            scene.rootNode.addChildNode(node)
        }
        
        sceneView.allowsCameraControl = true
        sceneView.autoenablesDefaultLighting = false
        sceneView.scene = scene
        
    }
    
    
    private func addObjects2() {
        
        let scene = SCNScene()
        for i in 0...(roomData.allItems.endIndex - 1) {
            let node = roomData.allItems[i].node
            scene.rootNode.addChildNode(node)
        }
        
        sceneView.allowsCameraControl = true
        sceneView.autoenablesDefaultLighting = false
        sceneView.scene = scene
        
        
//        guard var floorNode = getFloorNode() else { return }
//        scene.rootNode.addChildNode(floorNode)
//        sceneView.pointOfView = floorNode
        
        
    }
    
    func getFloorNode() -> SCNNode? {
        
        let cube = SCNBox(width: 10, height: 0, length: 10, chamferRadius: 0)
        let material = SCNMaterial()
        material.diffuse.contents = UIColor.blue
        cube.materials = [material]
        
        let node = SCNNode()
        node.geometry = cube
        
        var walls = roomData.walls ?? []
        var wallPositions = (roomData.walls ?? []).map({$0.node.position})
        var minX = wallPositions.compactMap({$0}).min(by: { ($0.x < $1.x )})?.x ?? 0
        var minY = wallPositions.compactMap({$0}).min(by: { ($0.y > $1.y) })?.y ?? 0
        var minZ = wallPositions.compactMap({$0}).min(by: { ($0.z < $1.z) })?.z ?? 0
        
        
        print("minx", minX)
        print("miny", minY)
        print("minz", minZ)
        
        //test purpose
        node.position = SCNVector3(x: 1.2660633, y: -2.48, z: -2.3593292)
        node.scale = SCNVector3(x: 0.99999994, y: 1.0, z: 1.0)
        //        node.transform = SCNMatrix4(m11: 0.56720567, m12: 0.0, m13: 0.82357615, m14: 0.0, m21: 0.0, m22: 1.0, m23: 0.0, m24: 0.0, m31: -0.82357615, m32: 0.0, m33: 0.56720567, m34: 0.0, m41: 1.2660633, m42: -0.13875899, m43: -2.3593292, m44: 1.0)
        node.orientation = SCNVector4(x: 0.0, y: 0.8852133, z: 0.0, w: 0.4651852)
        
        return node
        
    }
}

extension RoomModelController: SCNSceneRendererDelegate {
    
    override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
        /*
         1. Get The Current Touch Location
         2. Check That We Have Touched A Valid Node
         3. Check If The Node Has A Name
         4. Handle The Touch
         */
        
        guard let touchLocation = touches.first?.location(in: self.sceneView),
              let hitNode = sceneView?.hitTest(touchLocation, options: nil).first?.node,
              let identifier = hitNode.name,
              let node = self.roomData.allItems.first(where: { $0.identifier == identifier })
        else {
            // No Node Has Been Tapped
            return
            
        }
        
        // Handle Event Here e.g. PerformSegue
        print(node)
        //        self.domainModel.onItemTapForSearch.send(node.seachKey)
    }
    
}
//        let camera = SCNCamera()
//
////        camera.automaticallyAdjustsZRange = true
//        camera.fieldOfView = 100
//        camera.wantsExposureAdaptation = true
//        camera.sensorHeight = 200
//        //        camera.focalLength = 45
//        scene.rootNode.camera = camera
//
//        sceneView.defaultCameraController.maximumVerticalAngle = 5
//        sceneView.defaultCameraController.pointOfView =

//        let config = SCNCameraControlConfiguration()
//        config.autoSwitchToFreeCamera = false
//        config.allowsTranslation = false
//        config.flyModeVelocity = 100
//        sceneView.cameraControlConfiguration = config
