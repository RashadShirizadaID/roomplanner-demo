//
//  RoomCaptureController.swift
//  RoomPlanner
//
//  Created by Rashad Shirizada on 13.10.22.
//

import UIKit
import RoomPlan

class RoomCaptureController: UIViewController {
    @IBOutlet var doneButton: UIBarButtonItem!
    @IBOutlet var cancelButton: UIBarButtonItem!
    @IBOutlet weak var exportButton: UIButton!
    @IBOutlet weak var buttonsStack: UIStackView!
    
    private var roomCaptureView: RoomCaptureView!
    private var roomCaptureSessionConfig: RoomCaptureSession.Configuration = RoomCaptureSession.Configuration()
    private var isScanning: Bool = false
    private var roomBuilder: RoomBuilder!
    
    private var finalResults: CapturedRoom?

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        startSession()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        stopSession()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupRoomCaptureView()
        
        roomBuilder = RoomBuilder(options: [.beautifyObjects])
    }
    
    @IBAction func exportResults(_ sender: UIButton) {
        print("Export results")
        let destinationURL = FileManager.default.temporaryDirectory.appending(path: "Room.usdz")
        do {
            try finalResults?.export(to: destinationURL, exportOptions: .parametric)
            
            let activityVC = UIActivityViewController(activityItems: [destinationURL], applicationActivities: nil)
            activityVC.modalPresentationStyle = .popover
            
            present(activityVC, animated: true, completion: nil)
            if let popOver = activityVC.popoverPresentationController {
                popOver.sourceView = self.exportButton
            }
        } catch {
            print("Error = \(error)")
        }
    }
    
    @IBAction func showDetails(_ sender: UIButton) {
        if let capturedRoom = finalResults,  let resultController = self.storyboard?.instantiateViewController(withIdentifier: "RoomResultController") as? RoomResultController {
            resultController.capturedRoom = capturedRoom
            self.present(resultController, animated: true)
        }
    }
    
    @IBAction func doneScanning(_ sender: UIBarButtonItem) {
        if isScanning {
            stopSession()
        }
        else {
            cancelScanning(sender)
        }
    }
    
    @IBAction func cancelScanning(_ sender: UIBarButtonItem) {
        navigationController?.dismiss(animated: true)
    }
    
    private func setupRoomCaptureView() {
        roomCaptureView = RoomCaptureView(frame: view.bounds)
        roomCaptureView.captureSession.delegate = self
        roomCaptureView.delegate = self
        
        view.insertSubview(roomCaptureView, at: 0)
    }
    
    func startSession() {
        isScanning = true
        self.roomCaptureView.captureSession.run(configuration: roomCaptureSessionConfig)
        
        setActiveNavBar()
    }
    
    func stopSession() {
        isScanning = false
        self.roomCaptureView.captureSession.stop()
        
        setCompleteNavBar()
    }
    
    private func setActiveNavBar() {
        UIView.animate(withDuration: 1.0, animations: {
            self.cancelButton.tintColor = .white
            self.doneButton.tintColor = .white
            self.buttonsStack.alpha = 0.0
        }, completion: { complete in
            self.buttonsStack.isHidden = true
        })
    }
    
    private func setCompleteNavBar() {
        self.buttonsStack.isHidden = false
        UIView.animate(withDuration: 1.0) {
            self.cancelButton.tintColor = .systemBlue
            self.doneButton.tintColor = .systemBlue
            self.buttonsStack.alpha = 1.0
        }
    }
}
extension RoomCaptureController: RoomCaptureSessionDelegate, RoomCaptureViewDelegate {
   
    func captureView(shouldPresent roomDataForProcessing: CapturedRoomData, error: Error?) -> Bool {
        return true
    }
    
    func captureView(didPresent processedResult: CapturedRoom, error: Error?) {
        self.finalResults = processedResult
    }
    
    func captureSession(_ session: RoomCaptureSession, didEndWith data: CapturedRoomData, error: Error?) {
//        if let error = error {
//            print("Error:", error.localizedDescription)
//        }
//        
//        Task {
//            let finalRoom = try! await roomBuilder.capturedRoom(from: data)
//            print("FINAL ROOM", finalRoom.objects[0].confidence.)
//        }
    }
}
