//
//  RoomResultController.swift
//  RoomPlanner
//
//  Created by Rashad Shirizada on 13.10.22.
//

import UIKit
import RoomPlan

class RoomResultController: UIViewController {
    
    @IBOutlet weak var itemsTable: UITableView!
    var capturedRoom: CapturedRoom?
    
    private var totalObjects: [String: [Any]] = [:]
    private var keys: [String] {
        return Array(totalObjects.keys)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.view.backgroundColor = .white
        itemsTable.delegate = self
        itemsTable.dataSource = self
        
        guard let capturedRoom = capturedRoom
        else{ return }
        self.totalObjects.updateValue(capturedRoom.doors, forKey: "Door")
        self.totalObjects.updateValue(capturedRoom.windows, forKey: "Window")
        self.totalObjects.updateValue(capturedRoom.openings, forKey: "Openings")
        self.totalObjects.updateValue(capturedRoom.walls, forKey: "Walls")
        self.totalObjects.updateValue(capturedRoom.objects, forKey: "Objects")
        
        self.totalObjects = self.totalObjects.filter({!$0.value.isEmpty})
        
        self.itemsTable.reloadData()
    }
    
}

extension RoomResultController: UITableViewDelegate, UITableViewDataSource {
    
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return totalObjects.count
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let key = self.keys[section]
        
        let label = UILabel()
        label.text = key
        label.textColor = .red
        label.font = .boldSystemFont(ofSize: 14)
        
        return label
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        let key = self.keys[section]
        let values = self.totalObjects[key]
        return values?.count ?? 0
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let key = self.keys[indexPath.section]
        let value = self.totalObjects[key]?[indexPath.row]
        
        if value as? CapturedRoom.Surface != nil {
            let cell = tableView.dequeueReusableCell(withIdentifier: "surfaceCell") as! RoomSurfaceCell
            cell.updateUI(surface: value as! CapturedRoom.Surface)
            return cell
        }
        
        else if value as? CapturedRoom.Object != nil {
            let cell = tableView.dequeueReusableCell(withIdentifier: "objectCell") as! RoomObjectCell
            cell.updateUI(object: value as! CapturedRoom.Object)
            return cell
        }
        
        return UITableViewCell()
    }
}
